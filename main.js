var screenWidth = 640,
    screenHeight = 400,
    scale = 3,
    animationSpeed = 1,
    level = 1,
    lives = 3,
    totalScore = 0;



var game = new Phaser.Game(screenWidth, screenHeight, Phaser.AUTO, '', null, false, false);


var createPlayer = (projectiles) => {
    var player = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY * 1.75, 'player');
    player.anchor.setTo(0.5,0.5);
    player.scale.setTo(scale);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.collideWorldBounds = true;
    player.projectiles = projectiles || [];
    player.cooldown = 0;
    player.bonusCooldown = 0;
    return player;
};

var createAttacker = (x, y, tier) => {
    var spritesheet = "enemy" + tier;
    var enemy = game.add.sprite(x, y, spritesheet);
    enemy.anchor.setTo(0.5,0.5);
    enemy.scale.setTo(scale);
    game.physics.enable(enemy, Phaser.Physics.ARCADE);
    enemy.animations.add('movement', [0,1], animationSpeed, true);
    enemy.animations.play('movement');
    enemy.pointsMultiplier = tier % 3 + 1;
    return enemy;
};

var createAttackers = () => {
    var enemies = [],
        rowSize = 10,
        spaceW = (14 * scale),
        spaceH = (10 * scale);
    for(var row = 0; row < 3; row++){
        for (var col = 0; col < rowSize; col++) {
            enemies.push(createAttacker(spaceW *  (col + 1), spaceH * (row + 1), row + 1));
        }
    }
    return enemies;
};

var createProjectile = (x, y, velocity) => {
    var projectile = game.add.sprite(x, y, 'projectile');
    projectile.scale.setTo(scale);
    projectile.anchor.setTo(0.5,0.5);
    projectile.velocity = velocity;
    game.physics.enable(projectile, Phaser.Physics.ARCADE);
    return projectile;
};

var createExplosion = (x, y) => {
    var explosion = game.add.sprite(x, y, 'explosion');
    explosion.scale.setTo(scale);
    explosion.anchor.setTo(0.5,0.5);
    explosion.animations.add('explode', [0,1], animationSpeed, false);
    explosion.animations.play('explode', animationSpeed * 4, false, true);
};

var createBonusShip = (velocity) => {
    var bonusShip = game.add.sprite(0, 15, 'bonus');
    bonusShip.scale.setTo(scale);
    bonusShip.anchor.setTo(0.5, 0.5);
    bonusShip.velocity = velocity + (0.1 * level);
    game.physics.enable(bonusShip, Phaser.Physics.ARCADE);
    return bonusShip;
}

var getPlayerInput = (player) => {
    var speed = 5;
    if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT)){
        player.x = player.x - speed;
    }
    if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)){
        player.x = player.x + speed;
    }
    if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && player.cooldown <= 0){
        player.cooldown = player.bonusCooldown > 0 ? 20 : 40;
        player.projectiles.push(createProjectile(player.x, player.y - 0.5 * scale * 8,-6));
    }
};

var moveProjectiles = (projectiles) => {
    for(var i = 0; i < projectiles.length; i ++){
        if(projectiles[i].y < 0 || projectiles[i].y > screenHeight){
            projectiles[i].destroy();
            projectiles.splice(i, 1);
        }
        else {
            projectiles[i].y += projectiles[i].velocity;
        }
    }
};

var enemyActions = (enemies) => {
    var temp = enemies.attackers;
    var directionChange = false;

    for(var i = 0; i < temp.length; i++)
    {
        // poruszanie się
        if(enemies.velocity.yTimer > 1) {
            temp[i].y += Math.abs(enemies.velocity.value);
        }
        else {
            temp[i].x += enemies.velocity.value;
        }

        // sprawdzanie czy atakujący dotknęli brzegu ekranu i zmiana kierunku poruszania
        if(temp[i].x - (scale*6) <= 0 || temp[i].x + (scale*6) >= screenWidth){
            directionChange = true;
        }

        // Decyzja o strzale
        var rnd = Math.random();
        if(rnd < enemies.fireProjectileChance) {
            enemies.projectiles.push(createProjectile(temp[i].x, temp[i].y, 6));
        }
    }

    // zmniejsz timer poruszania w dół o 1
    if(enemies.velocity.yTimer > 0) {
        enemies.velocity.yTimer--;
    }
    // lub rozpocznij zmniejszanie dystansu do gracza przez 30 klatek i zmień kierunek
    else if (directionChange){
        enemies.velocity.yTimer = 30;
        enemies.velocity.value *= -1;
    }

    moveProjectiles(enemies.projectiles);
};

var playerActions = (player) => {
    if(player.cooldown > 0) player.cooldown--;
    if(player.bonusCooldown > 0) player.bonusCooldown--;
    getPlayerInput(player);
    moveProjectiles(player.projectiles);
};

var handleCollisions = (state) => {
    // wróg trafiony
    game.physics.arcade.collide(state.player.projectiles, state.enemies.attackers, (proj, e) => {
        createExplosion(e.x, e.y);
        e.destroy();
        state.enemies.attackers.splice(state.enemies.attackers.indexOf(e), 1);
        proj.destroy();
        state.player.projectiles.splice(state.player.projectiles.indexOf(proj), 1);
        // po każdym zniszczeniu wroga zwiększ szybkość i prawdopodobieństwo strzału dla reszty
        state.enemies.velocity.value *= 1.05;
        state.enemies.fireProjectileChance *= 1.05;
        // punktacja
        state.playerScore.value += 10 * e.pointsMultiplier;
        state.playerScore.pointsChanged = true;
    });
    // zestrzelenie statku bonusowego
    game.physics.arcade.collide(state.player.projectiles, state.bonusShip, (proj, bs) =>{
        createExplosion(bs.x, bs.y);
        proj.destroy();
        state.player.projectiles.splice(state.player.projectiles.indexOf(proj), 1);
        bs.destroy();

        // ustaw cooldown z bonusem dla gracza
        state.player.bonusCooldown = 500;
        state.player.cooldown = 0;
        state.playerScore.value += 300;
        state.playerScore.pointsChanged = true;
        lives++;
    });
    // gracz trafiony pociskiem
    game.physics.arcade.collide(state.player, state.enemies.projectiles,  (pl, proj)=>{
        createExplosion(player.x, player.y);
        pl.destroy();
        proj.destroy();
        state.enemies.projectiles.splice(state.enemies.projectiles.indexOf(proj), 1);
        lives--;
        state.player = (lives >= 0) ? createPlayer(state.player.projectiles) : null;
    });
    // gracz - kolizja z wrogiem
    game.physics.arcade.collide(state.player, state.enemies.attackers,  (pl, a)=>{
        createExplosion(player.x, player.y);
        pl.destroy();
        a.destroy();
        state.enemies.attackers.splice(state.enemies.attackers.indexOf(a), 1);
        state.player = null;
    });
};

var bonusShipAction = (state) =>{
    // stwórz statek bonusowy

    if(state.bonusShip != null) state.bonusShip.x += bonusShip.velocity;

    if(state.bonusShipCooldown < 0){
        return;
    }
    else if(state.bonusShipCooldown == 0){
        state.bonusShip = createBonusShip(1);
        state.bonusShipCooldown--;
    }
    else{
        state.bonusShipCooldown--;
    }
};

var mainState = {
    preload: () => {
        game.load.image('player', 'assets/images/player.png');
        game.load.image('background', 'assets/images/black.png');
        game.load.image('projectile', 'assets/images/white.png');
        game.load.image('bonus', 'assets/images/ship.png');
        game.load.spritesheet('enemy1', 'assets/images/enemy1.png',8,8);
        game.load.spritesheet('enemy2', 'assets/images/enemy2.png',10,8);
        game.load.spritesheet('enemy3', 'assets/images/enemy3.png',12,8);
        game.load.spritesheet('explosion', 'assets/images/explosion.png',13,8);
        game.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png',
            'assets/fonts/carrier_command.xml');
    },
    create: () => {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.background = game.add.sprite(0, 0, 'background');
        this.background.scale.setTo(Math.max(screenWidth, screenHeight));

        this.player = createPlayer();

        this.enemies = {
            attackers: createAttackers(),
            projectiles: [],
            velocity: { value: 0.4 + level * 0.1 , yTimer: 0 },
            fireProjectileChance: level * 0.0001
        };

        this.playerScore = game.add.bitmapText(3, 3, 'carrier_command', 'Points: 0', 10);
        this.playerScore.value = 0;
        this.playerScore.pointsChanged = false;

        this.playerLives = game.add.bitmapText(3, screenHeight - 13, 'carrier_command', 'Lives: ' + lives, 10);
        this.playerLives.value = lives;

        this.bonusShipCooldown = 750;

    },
    update: () => {
        // Porażka gdy gracz nie istnieje lub gdy najeźdźcy osiągnęli dolną część mapy
        if(this.player == null || this.enemies.attackers.some((attacker) => attacker.y > screenHeight)) {
            totalScore += this.playerScore.value;
            game.state.start('GameOverState');
            return;
        }
        // sukces gdy wszyscy najeźdźcy zniszczeni
        else if(this.enemies.attackers.length === 0){
            totalScore += this.playerScore.value;
            level++; 
            game.state.start('NextLevelState');
            return;
        }

        playerActions(this.player);
        enemyActions(this.enemies);
        bonusShipAction(this);
        handleCollisions(this);

        // odśwież punktację
        if(this.playerScore.pointsChanged) {
            this.playerScore.text = "Points: " + this.playerScore.value;
            this.playerScore.pointsChanged = false;
        }

        // odśwież życia
        if(this.playerLives.value != lives) {

            this.playerLives.text = "Lives: " + (this.playerLives.value = lives);
        }
    },
};

var nextLevelState = {
    preload: () => {
        game.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png',
            'assets/fonts/carrier_command.xml');
    },
    create: () => {
        this.levelTextDisplay = game.add.bitmapText(screenWidth * 0.5, screenHeight * 0.4,
            'carrier_command', "Level " + level);
        this.levelTextDisplay.anchor.setTo(0.5,0.5);

        if(totalScore > 0){
            this.totalScoreDisplay = game.add.bitmapText(screenWidth * 0.5, screenHeight * 0.6,
                'carrier_command', "Total score: " + totalScore, 15);
            this.totalScoreDisplay.anchor.setTo(0.5,0.5);
        }

        this.pressSpaceDisplay = game.add.bitmapText(screenWidth * 0.5, screenHeight * 0.8,
            'carrier_command', "Press <ENTER> to continue", 20);
        this.pressSpaceDisplay.anchor.setTo(0.5,0.5);
    },
    update: () => {
        if(game.input.keyboard.isDown(Phaser.Keyboard.ENTER)){
            game.state.start('MainState');
        }
    }
};

var gameOverState = {
    preload: () => {
        game.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png',
            'assets/fonts/carrier_command.xml');
    },
    create: () => {
        this.levelTextDisplay = game.add.bitmapText(screenWidth * 0.5, screenHeight * 0.4,
            'carrier_command', "Game Over");
        this.levelTextDisplay.anchor.setTo(0.5,0.5);

        if(totalScore > 0){
            this.totalScoreDisplay = game.add.bitmapText(screenWidth * 0.5, screenHeight * 0.6,
                'carrier_command', "Total score: " + totalScore, 15);
            this.totalScoreDisplay.anchor.setTo(0.5,0.5);
        }

        this.pressSpaceDisplay = game.add.bitmapText(screenWidth * 0.5, screenHeight * 0.8,
            'carrier_command', "Press <ENTER> to try again", 20);
        this.pressSpaceDisplay.anchor.setTo(0.5,0.5);
    },
    update: () => {
        if(game.input.keyboard.isDown(Phaser.Keyboard.ENTER)){
            level = 1;
            totalScore = 0;
            lives = 3;
            game.state.start('NextLevelState');
        }
    }
};


game.state.add('MainState', mainState);
game.state.add('NextLevelState', nextLevelState);
game.state.add('GameOverState', gameOverState);

game.state.start('NextLevelState');