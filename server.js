var express = require('express');
var app = express();

// folder assets jako statyczny
app.use('/assets', express.static('assets'));

// routy dla strony
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});
app.get('/index', (req, res) => {
    res.sendFile(__dirname + '/index.html');
}); 
app.get('/index.html', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// routy dla skryptów
app.get('/phaser.js', (req, res) => {
    res.sendFile(__dirname + '/phaser.js');
});
app.get('/phaser.min.js', (req, res) => {
    res.sendFile(__dirname + '/phaser.min.js');
});
app.get('/main.js', (req, res) => {
    res.sendFile(__dirname + '/main.js');
});

app.listen(3000, () => {
    console.log('Space invaders app listening on port 3000, press Ctrl-C to abort.');
});